import { log } from "./logger";

type Slot =
  | 1
  | 2
  | 3
  | 4
  | 5
  | 6
  | 7
  | 8
  | 9
  | 10
  | 11 /** Pill */
  | 12 /** Vial */;

interface IProcess {
  pointer: number;
  cells: number[];
  reservoirs: number[];
  sx: number;
  tx: number;
  ax: number;
  instructions: string[];

  // Basic pointer manipulation and arithmetic
  moveToCell(cell: number): IProcess;
  setCellValue(value: number): IProcess;

  fetchOrSetValue(value: number): IProcess;

  // Register operations

  setSX(): IProcess;
  setTX(): IProcess;
  setAX(): IProcess;

  copySX(): IProcess;
  copyTX(): IProcess;
  copyAX(): IProcess;

  resetRegisters(): IProcess;
  /**
   *
   * @param kelvin Set the temperature to decide level
   * @param slot Which slot to target
   */
  setTemperature(kelvin: number, slot?: Slot): IProcess;

  transfer(): IProcess;
  transferUnits(from: Slot, to: Slot, amonut: number): IProcess;

  compile(): string;
}

const Process: IProcess = {
  pointer: 0,
  cells: new Array(1024).fill(0),
  reservoirs: new Array(10).fill(0),
  instructions: [],

  sx: 0,
  tx: 0,
  ax: 0,

  moveToCell(cell: number) {
    // Use abs to get distance between the pointer and the cell
    const distance = Math.abs(this.pointer - cell);
    // Check which is greater to determine the direction of the cell
    const direction = this.pointer > cell ? "<" : ">";
    log("distance:", distance, "direction:", direction);
    //Create new array and fill it with distance, then join all
    const command = new Array(distance).fill(direction).join("");
    this.instructions.push(command);
    //log(this.instructions.join(""));
    this.pointer = cell;
    return this;
  },

  setCellValue(value: number) {
    const currentValue = this.cells[this.pointer];
    const differnece = Math.abs(currentValue - value);
    const alterator = currentValue < value ? "+" : "-";
    log("difference:", differnece, "alterator:", alterator);

    const command = new Array(differnece).fill(alterator).join("");

    this.instructions.push(command);
    this.cells[this.pointer] = value;
    log("set cell #", this.pointer, "from", currentValue, "to", value);
    //log(this.instructions.join(""));
    return this;
  },

  /**
   * Tries to find the closest value to save space, otherwise does a setCellValue method
   *
   * @param value
   */
  fetchOrSetValue(value: number) {
    return this;
  },

  setSX() {
    this.sx = this.cells[this.pointer];
    this.instructions.push("}");
    return this;
  },

  setTX() {
    this.tx = this.cells[this.pointer];
    this.instructions.push(")");
    return this;
  },

  setAX() {
    this.ax = this.cells[this.pointer];
    this.instructions.push("'");
    return this;
  },

  copySX() {
    this.cells[this.pointer] = this.sx;
    this.instructions.push("{");
    return this;
  },

  copyTX() {
    this.cells[this.pointer] = this.tx;
    this.instructions.push("(");
    return this;
  },

  copyAX() {
    this.cells[this.pointer] = this.ax;
    this.instructions.push("^");
    return this;
  },

  resetRegisters() {
    this.setCellValue(0).setSX().setTX().setAX();
    return this;
  },

  // $ Heat or cool reagent in sx to ((273 - tx) + ax) Kelvin. Heating takes as long as the chemistry lab heater would take.

  setTemperature(kelvin: number, slot?: Slot) {
    // ((273 - tx) + ax)
    this.resetRegisters();
    // Set SX
    if (slot) this.moveToCell(0).setCellValue(slot).setSX();
    //Reset sx,tx
    this.moveToCell(1).setCellValue(0).setTX().setAX();
    this.moveToCell(2);
    if (kelvin > 273) {
      this.setCellValue(kelvin - 273);
      this.setAX();
    } else if (kelvin < 273) {
      this.setCellValue(273 - kelvin);
      this.setTX();
    }
    this.instructions.push("$");
    return this;
  },

  transfer() {
    this.instructions.push("@");
    return this;
  },

  transferUnits(from: Slot, to: Slot, amount: number) {
    this.resetRegisters();

    this.moveToCell(0)
      .setCellValue(from)
      .setSX()
      .moveToCell(1)
      .setCellValue(to)
      .setTX()
      .moveToCell(2)
      .setCellValue(amount)
      .setAX()
      .transfer();
    return this;
  },

  compile() {
    let code = this.instructions.join("");

    // Optimse whatever we can

    //Return "better" code
    return code;
  },
};

export default Process;
