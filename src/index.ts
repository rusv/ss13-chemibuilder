import { log } from "./logger";
import Process from "./process";
const p = Process;

(async () => {
  p.setTemperature(200, 1); // Water
  p.setTemperature(200, 2); // Pottasium
  //p.setTemperature(200, 3); // Bromine
  p.transferUnits(1, 4, 50); //Ice
  p.transferUnits(2, 4, 50);
  //p.transferUnits(3, 4, 33);

  p.transferUnits(6, 10, 80); // Napalm Goo
  //p.transferUnits(7, 10, 25); // Oil
  p.setTemperature(200, 10);

  p.transferUnits(4, 10, 20); // Iciumine
  p.setTemperature(265, 10);

  p.transferUnits(10, 11, 100);

  const code = p.compile();
  log(code);
})();

// Hydrogen 1 (3) - Ammonia - Cyanide
// Nitrogen 2 (1) - Ammonia - Cyanide
// Hydrogen 6 - Oil - Cyanide
// Carbon 7 - Oil - Cyanide
// Welding Fuel 8 - Oil - Cyanide
// Oil 3 (Buffer) - Cyanide
// Oxygen 4 - Cyanide
// Ammonia 9 (Buffer) - Cyanide
// Cyanide 10 (Buffer)

/*p.transferUnits(1, 9, 30).transferUnits(2, 9, 10); // 60 ammonia 60
  p.transferUnits(6, 3, 30).transferUnits(7, 3, 30).transferUnits(8, 3, 30); // 66 Mix oil
  p.transferUnits(4, 10, 10).transferUnits(3, 10, 10).transferUnits(9, 10, 10);
  p.setTemperature(10, 400);*/

/*
P.setCellValue(1)
    .setSX()
    .moveToCell(1)
    .setCellValue(2)
    .setTX()Shinji-Kneecaps
    .moveToCell(0)
    .setCellValue(10)
    .setAX()
    .transfer();
*/
