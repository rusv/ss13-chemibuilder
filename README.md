# SS13 Chemibuilder

Uses typescript to manage chemistry automation in SS13 : https://wiki.ss13.co/ChemiCompiler

Used to simplify Brainfuck : https://en.wikipedia.org/wiki/Brainfuck


All methods return current process object so that the build process can be chained.

## Methods
### Basic pointer manipulation and arithmetic 
Handles adding, subtracting cells and moving the pointer.
###### Moves pointer to cell
```ts
moveToCell(cell: number): IProcess
```

###### Sets cell value at cell[pointer]
```ts
setCellValue(value: number): IProcess;
```

###### TODO: Does either moveToCell() or setCellValue(), depending on which is the most efficent to save space
```ts
fetchOrSetValue(value: number)
```
---
### Register operations
###### Sets Source / Target / Amount register to cell value at pointer (reverse of copy)
```ts
setSX(): IProcess;
setTX(): IProcess;
setAX(): IProcess;
```

###### Copies Source / Target / Amount register to cell at pointer (reverse of set)
```ts
copySX(): IProcess;
copyTX(): IProcess;
copyAX(): IProcess;
```

#### Resets all registers(sx,tx and ax) to 0
```ts
resetRegisters(): IProcess;
```

### Heating operations
#### Set the temperature in kelvin, slot targets Target-TX if not specified
```ts
setTemperature(kelvin: number, slot?: Slot): IProcess;
```

### Transfer operations
#### Transfers from slot (Source-SX) to (Target-TX) with (Amount-AX)
```ts
transfer(): IProcess;
```

#### Does the same as transfer but does the register handling automaticly
```ts
transferUnits(from: Slot, to: Slot, amonut: number): IProcess;
```

### Finalizing operations:
#### Returns a string with the finished ChemiFuck code, use this in the ChemiCompiler
```ts
compile(): string
```
---
## Examples
#### Move and mix two containers into a third container (Like water + pottasium)
```ts
import Process from './process'
const p = Process;

// 1 = Water and 2 = Pottasium > 3 = BOOM
p.transferUnits(1, 3, 50).transferUnits(2, 3, 50);
```

#### Set the temperature of containers before mixing them (Like water > ice + pottasium)
```ts
import Process from './process'
const p = Process;

// 1 = Water > Ice and 2 = Pottasium > 3 = Delayed boom
p.setTemperature(1, 1).transferUnits(1, 3, 50); // Freezes the water to 1 kelvin so it doesn't react with pottasium
p.transferUnits(2, 3, 50);
```

